import React from 'react';
import {
    Link
  } from "react-router-dom";
class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() { }
  render() {
    return(
      <div>
        <h2>SideBar If You Want to use routing
        </h2>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/create">Create</Link>
          </li>
          <li>
            <Link to="/update">Update</Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default SideBar;