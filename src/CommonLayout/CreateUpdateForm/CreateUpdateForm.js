import React from 'react';
import './CreateUpdateForm.css';
import * as validator from '../../Util/Validation';

class CreateUpdateForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            productName: '',
            price: '',
            errors: {
                productName: undefined,
                price: undefined,
            }
        };
    }

    componentDidMount() { }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.productState.id !== prevProps.productState.id) {
            this.setState(this.props.productState);
        }
    }
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'productName':
                errors.productName =
                validator.required(value, event);
                break;
            case 'price':
                errors.price =
                validator.required(value, event);
                break;
            default:
                break;
        }

        this.setState({ errors, [name]: value });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (validator.validateForm(this.state.errors)) {
            let productState = this.props.productState;
            if (productState.id) {
                this.props.updateElement(this.state);
                alert('Update Success');
            } else {
                this.props.addNewElement(this.state);
                alert('Add Success');
            }
        } else {
            alert('Invalid Form');
        }
    }

    render() {
        const { errors } = this.state;
        const { productName, price } = this.state;
        // this.state = this.props.productState;
        return (
            <form onSubmit={this.handleSubmit}
                noValidate>
                <div className="row">
                    <div className="col-25">
                        <label htmlFor="productName">Product Name</label>
                    </div>
                    <div className="col-75">
                        <Input placeholder="Product Name"
                            type="text"
                            name="productName"
                            value={this.state.productName}
                            onChange={(event) => this.handleChange(event)}
                        />
                        {errors.productName}
                    </div>
                </div>
                <div className="row">
                    <div className="col-25">
                        <label htmlFor="price">Price</label>
                    </div>
                    <div className="col-75">
                        <Input placeholder="Price"
                            type="number"
                            name="price"
                            value={this.state.price}
                            onChange={(event) => this.handleChange(event)}
                        />
                        {errors.price}
                    </div>
                </div>
                {
                    this.state.id ?
                        <div className="row marginTop10Px">
                            <input type="submit" value="Update" />
                        </div> :
                        <div className="row marginTop10Px">
                            <input type="submit" value="Submit" />
                        </div>
                }

            </form>
        )
    }
}

export default CreateUpdateForm;

// Define own Input component
const Input = ({ error, isChanged, isUsed, ...props }) => (
    <div>
        <input {...props} />
    </div>
);

