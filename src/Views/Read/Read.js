import React from 'react';
import {
    SideBar as SideBarView,
} from '../../CommonLayout';
import './Read.css';

class Read extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() { }
    render() {
        const { products } = this.props;
        return (
            <div className="container">
                <h1>Read</h1>
                <table>
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map(product => (
                            <tr key={product.id}>
                                <td>{product.id}</td>
                                <td>{product.productName}</td>
                                <td>{product.price} $</td>
                                <td>
                                    <button variant="info" onClick={() => this.props.editProduct(product, 'Update')}>Edit</button>
                                    &nbsp;<button variant="danger" onClick={() => this.props.deleteAnElement(product)}>Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Read;