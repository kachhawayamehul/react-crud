import React from 'react';
import {
    SideBar as SideBarView,
} from '../../CommonLayout';
import {
    Read as ReadView,
    Create as CreateView,
    Update as UpdateView,
} from '../../Views';
import './Home.css';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            currentState: 'Read',
            selectedProduct: ''
        }
    }

    componentDidMount() {
        let initialData = [
            {
                id: 1,
                productName: 'Product-1',
                price: '2'
            },
            {
                id: 2,
                productName: 'Product-2',
                price: '3'
            },
            {
                id: 3,
                productName: 'Product-3',
                price: '5'
            },
        ]
        this.setState({ products: initialData })
    }
    editProduct = (selectedProduct, state) => {
        this.setState({ currentState: state, selectedProduct: selectedProduct })
    }
    addNewElement = (addedElement) => {
        let _products = this.state.products;
        // ***** Set Id In Incremented
        let _id = 0;
        if (_products.length)
            _id = (_products[_products.length - 1].id) + 1;
        else
            _id = 1;

        let _addedElement = { id: _id, productName: addedElement.productName, price: addedElement.price };
        _products = [..._products, _addedElement];
        this.setState({ products: _products });
    }
    updateElement = (updatedElement) => {
        let _products = Object.assign([], this.state.products);
        let _index = this.getIndexOfArrayData(_products, 'id', updatedElement.id);
        if (-1 != _index) {
            _products[_index].productName = updatedElement.productName;
            _products[_index].price = updatedElement.price;
            this.setState({ products: _products, currentState: 'Read' });
        } else {
            alert('Invalid Operation');
            this.setState({ currentState: 'Read' });
        };
    }
    deleteAnElement = (element) => {
        let _products = Object.assign([], this.state.products);
        let _index = this.getIndexOfArrayData(_products, 'id', element.id);
        if (-1 != _index) { _products.splice(_index, 1); this.setState({ products: _products }); };
    }
    getIndexOfArrayData = (data, property, value) => {
        let result = -1;
        data.some(function (item, i) {
            if (item[property] === value) {
                result = i;
                return true;
            }
        });
        return result;
    }
    render() {
        const { products, currentState, selectedProduct } = this.state;
        return (
            <div>
                {/* <SideBarView /> */}
                <div className="container">
                    <h2>Home </h2>
                    {/* Show Read Button */}
                    {(currentState == 'Read') && <button variant="info" onClick={() => this.setState({ currentState: 'Create' })}>Create</button>}
                </div>
                {
                    // ***** Show Create View
                    currentState == 'Create' ?
                        <CreateView back={() => this.setState({ currentState: 'Read' })} addNewElement={(addedElement) => this.addNewElement(addedElement)} />
                        : (
                            // ***** Show Update View Else (If @currentState is not Create and Update It Automatic set as Read)
                            currentState == 'Update' ?
                                <UpdateView selectedProduct={selectedProduct} back={() => this.setState({ currentState: 'Read' })} updateElement={(updatedElement) => this.updateElement(updatedElement)} /> :
                                <ReadView products={products} editProduct={(selectedProduct, state) => this.editProduct(selectedProduct, state)} back={() => this.setState({ currentState: 'Read' })} deleteAnElement={(element) => this.deleteAnElement(element)} />
                        )
                }
            </div>
        )
    }
}

export default Home;