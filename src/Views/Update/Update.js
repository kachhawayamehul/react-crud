import React from 'react';
import {
    SideBar as SideBarView,
    CreateUpdateForm as CreateUpdateFormView,
} from '../../CommonLayout';
import './Update.css';

class Update extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            productName: '',
            price: '',
            errors: {
                productName: undefined,
                price: undefined,
            }
        };
    }

    componentDidMount() {
        let selectedProduct = this.props.selectedProduct;
        this.setState({
            id: selectedProduct.id,
            productName: selectedProduct.productName,
            price: selectedProduct.price,
            errors: {
                productName: false,
                price: false,
            }
        });
    }
    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-25">
                            <h1>Update</h1>
                        </div>
                        <div className="col-75">
                            {/* Back To Read View */}
                            <button className="backBtn" variant="danger" onClick={() => this.props.back('Back')}>Back</button>
                        </div>
                    </div>
                    <CreateUpdateFormView productState={this.state} updateElement={(updatedElement) => this.props.updateElement(updatedElement)} />
                </div>
            </div>
        )
    }
}

export default Update;