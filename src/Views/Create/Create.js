import React from 'react';
import {
    SideBar as SideBarView,
    CreateUpdateForm as CreateUpdateFormView,
} from '../../CommonLayout';
import './Create.css';

const validateForm = (errors) => {
    // ***** val == undefined means value is blank set valid as false
    // ***** OR
    // ***** val != undefined means Something is there && if it's not val==false means field is invalid 
    let valid = true;
    Object.values(errors).forEach(
        (val) => (val == undefined && (valid = false)) || (val != undefined && val != false && (valid = false))
    );
    return valid;
}
class Create extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            productName: '',
            price: '',
            errors: {
                productName: undefined,
                price: undefined,
            }
        };
    }

    componentDidMount() { }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'productName':
                errors.productName =
                    required(value, event);
                break;
            case 'price':
                errors.price =
                    required(value, event);
                break;
            default:
                break;
        }

        this.setState({ errors, [name]: value });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (validateForm(this.state.errors)) {
            this.props.addNewElement(this.state);
            // console.info('Valid Form');
        } else {
            // console.error('Invalid Form');
        }
    }

    render() {
        const { errors } = this.state;
        return (
            <div>
                <div className="container">

                    <div className="row">
                        <div className="col-25">
                            <h1>Create</h1>
                        </div>
                        <div className="col-75">
                            {/* Back To Read View */}
                            <button className="backBtn" variant="danger" onClick={() => this.props.back('Back')}>Back</button>
                        </div>
                    </div>
                    <CreateUpdateFormView productState={this.state} addNewElement={(addedElement) => this.props.addNewElement(addedElement)} />
                </div>
            </div>
        )
    }
}

export default Create;

// Define own Input component
const Input = ({ error, isChanged, isUsed, ...props }) => (
    <div>
        <input {...props} />
    </div>
);

const required = (value, props) => {
    if (!value || (props.isCheckable && !props.checked)) {
        return <span style={{ color: "red" }}>Required</span>;
    } else {
        return false
    }
};