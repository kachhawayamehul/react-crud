import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import {
  Home as HomeView,
  Create as CreateView,
  Update as UpdateView,
} from './Views';

const Routes = () => {
  return (
    <Switch>
        <Route exact path="/">
            <HomeView />
        </Route>
        <Route exact path="/create">
            <CreateView />
        </Route>
        <Route exact path="/update">
            <UpdateView />
        </Route>
    </Switch>
  );
};

export default Routes;
