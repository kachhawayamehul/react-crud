import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './Routes';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
const browserHistory = createBrowserHistory();

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {}

  render() {
    return(
      <Router history={browserHistory}>
        <Routes />
      </Router>
    )
  }
}

export default App;

