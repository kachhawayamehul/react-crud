import React from 'react';

export const validateForm = (errors) => {
    // ***** val == undefined means value is blank set valid as false
    // ***** OR
    // ***** val != undefined means Something is there && if it's not val==false means field is invalid 
    let valid = true;
    Object.values(errors).forEach(
        (val) => (val == undefined && (valid = false)) || (val != undefined && val != false && (valid = false))
    );
    return valid;
}

export const required = (value, props) => {
    if (!value || (props.isCheckable && !props.checked)) {
        return <span style={{ color: "red" }}>Required</span>;
    } else {
        return false
    }
};